var gulp = require('gulp');
var webpack = require('gulp-webpack');
var concat = require('gulp-concat');

var sources = [
  'node_modules/p5/lib/p5.js',
  'node_modules/p5/lib/addons/p5.dom.js',
  'src/sketch.js'
];

gulp.task('default', function() {
  return gulp.src(sources)
  .pipe(concat('sketch.js'))
  // .pipe(webpack({
  //     output: {
  //       filename: 'sketch.js',
  //     },
  //   }))
  .pipe(gulp.dest('./public/'));
});
