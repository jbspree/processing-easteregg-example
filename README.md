Processing Easteregg Example
============================

### Requirements

- git
- node.js (tested with v6.)
- npm (tested with v4.)

### Getting started


```bash
# clone the repository
git clone git@gitlab.com:jbspree/processing-easteregg-example.git

# change to the new directory
cd processing-easteregg-example

# install the npm recipe
npm install

# edit files as desired
# src/sketch.js
# public/index.html

# run the build
npm run build

#open index.html your browser

```
